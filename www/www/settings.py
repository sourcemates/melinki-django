import os

instance_name = os.getenv('INSTANCE_NAME', 'lin')

if instance_name == 'lin':
    from .settings_lin import *
if instance_name == 'staging':
    from .settings_staging import *
