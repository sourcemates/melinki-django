from .settings_base import *


TEST_REQUEST_DEFAULT_FORMAT = 'json'

class DisableMigrations(object):
    """
    Based on https://gist.github.com/NotSqrt/5f3c76cd15e40ef62d09

    Disable migrations
    """
    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return "notmigrations"

# MIGRATION_MODULES = DisableMigrations()
