from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from links.models import Link


class DrfViewTestCase(APITestCase):
    def setUp(self):
        Link.objects.create(
            url="http://wp.pl",
        )
        Link.objects.create(
            url="http://google.com",
        )

    def test_get_list_view(self):
        url = reverse('link-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['results']), 2)

    def test_get_detail_view(self):
        link = Link.objects.get(url='http://wp.pl')
        url = reverse('link-detail', kwargs={'pk': link.id})
        response = self.client.get(url)
        self.assertEqual(response.data['url'], 'http://wp.pl')
        self.assertIsInstance(response.data['id'], int)

    def test_post_view(self):
        url = reverse('link-list')
        data = dict(
            url='http://melinki.eu',
        )
        response = self.client.post(url, data)
        link = Link.objects.get(url='http://melinki.eu')
        self.assertEqual(link.url, 'http://melinki.eu')
        self.assertIsInstance(link.id, int)

    def test_put_view(self):
        link = Link.objects.get(url='http://google.com')
        url = reverse('link-detail', kwargs={'pk': link.id})
        data = dict(
            url='http://yandex.ru'
        )
        response = self.client.put(url, data)
        link.refresh_from_db()
        self.assertEqual(link.url, 'http://yandex.ru')
