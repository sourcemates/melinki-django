from django.test import Client
from django.test import TestCase
from rest_framework.reverse import reverse


class ViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_home_redirect(self):
        url = reverse('home')
        response = self.client.get(url, follow=True)
        self.assertRedirects(response, reverse('link-list'))
