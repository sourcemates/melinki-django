from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView


class HomeView(RedirectView):
    permanent = False
    url = reverse_lazy('link-list')
