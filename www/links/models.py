from django.db import models


class Link(models.Model):
    url = models.URLField(max_length=1000)
    image = models.ImageField(upload_to='images', null=True, blank=True)
