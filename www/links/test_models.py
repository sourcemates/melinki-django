from django.test import TestCase

from links.models import Link


class ModelTestCase(TestCase):
    def test_link_create(self):
        Link.objects.create(url='http://onet.pl')
        self.assertEqual(Link.objects.filter(url='http://onet.pl').count(), 1)
