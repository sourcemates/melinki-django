#!/bin/sh
cd /code/www && python manage.py migrate --noinput && python manage.py collectstatic --noinput
uwsgi --ini /code/uwsgi.ini
