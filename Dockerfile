FROM python:3.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/

# Run it
# ENTRYPOINT ["python", "www/manage.py", "runserver", "--settings=www.settings_staging", "0.0.0.0:8000"]
ENTRYPOINT ["uwsgi", "--ini", "/code/uwsgi.ini"]
# for some strange reason this doesn't work:
# ENTRYPOINT ["/code/scripts/deploy.sh"]

# Expose listen ports
EXPOSE 8000
