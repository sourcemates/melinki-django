# Melinki Django project

Staging version available under: http://django.melinki.eu/

Project provides an API.

## Tools

### Run locally using docker:

```
docker build -t melinki . && docker run -p 8000:8000 melinki
```

then open http://127.0.0.1:8000/ in your browser.
